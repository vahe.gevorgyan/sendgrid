<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MailController@test');
//Route::get('/test-email', 'MailController@index')->name('testMail');
Route::post('/send', 'MailController@sendMail')->name('sendMail');
Route::post('/event', 'MailController@postEmailEvents');
Route::get('/all-stats', 'MailController@getAllStats');
//Route::get('/sender-events/search', 'MailController@getSearch')->name('search');
//Route::get('/sender-events/{id}', 'MailController@getSgMessageEvents')->name('senderEvent');
Route::get('/sg-message', 'MailController@getOneSgMessageStats')->name('sgMessageEvents');
Route::get('/user/get-user', 'UserController@getUser');


Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/user/logout', 'UserController@logout')->name('logout');


Route::post('/mobi/add-mobi-webhook', 'MobiController@addMobiWebhook');
Route::get('/mobi/webhooks', 'MobiController@getWebhooks');
Route::post('/mobi/edit-mobi-webhook', 'MobiController@editMobiWebhook');
Route::post('/mobi/delete-mobi-webhook', 'MobiController@deleteMobiWebhook');
Route::post('/mobi/webhook-convert', 'MobiController@webHookConverter');
Route::get('/mobi/nps-records', 'MobiController@getNpsRecords');
Route::post('/mobi/manual-post', 'MobiController@manualPost');
Route::get('/xero/connect', 'XeroController@connect');
Route::get('/xero/connect-success', 'XeroController@connectSuccess');
Route::get('/xero/index', 'XeroController@index');
Route::get('/xero/contacts', 'XeroController@contacts');