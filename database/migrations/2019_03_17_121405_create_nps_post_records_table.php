<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNpsPostRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nps_post_records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mobi_webhook_id');
            $table->integer('mobi_post_record_id');
            $table->string('date_confirmed');
            $table->string('date_scheduled');
            $table->string('email');
            $table->string('status');
            $table->text('post_data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nps_post_records');
    }
}
