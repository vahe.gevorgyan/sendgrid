<!doctype html>
<html lang="en">
@include('header')
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<div class="container">
    <div class="row">
        @if($errors->any())
            <h4 style="color: red">{{$errors->first()}}</h4>
        @endif
    </div>
    <div class="row">
        <div class="col-2">
            <a href="{{route('testMail')}}">Home</a>
        </div>
        <div class="col-2">
            <a href="{{route('testMail')}}">Send Test</a>
        </div>
    </div>
    <div class="row justify-content-between">
        <div class="col-4">
            To: {{$email}}
        </div>
        <div class="col-4">
            <a href="{{route('allEvents')}}">Back</a>
        </div>
    </div>
    <div class="row">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Datetime</th>
                <th scope="col">Event Name</th>
                <th scope="col">SG_Message_ID</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sgMessageEvents as $event)
                <tr>
                    <td>{{ $event['date'] }}</td>
                    <td>{{ $event['event'] }}</td>
                    <td>{{ $event['sg_message_id'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@include('footer')
</body>
</html>