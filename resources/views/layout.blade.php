<!doctype html>
<html class="no-js " lang="en" ng-app="auditApp"  ng-controller="MainController">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="sQuare Bootstrap 4 Angular 5 admin dashboard with ui kit.">
    {{--<title data-ng-bind="$state.current.data.pageTitle"></title>--}}
    <title>Sendgrid webhook notifications</title>
    {{--<link rel="icon" href="favicon.ico" type="image/x-icon"> <!-- Favicon-->--}}
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    {{--<link rel="stylesheet" href="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css') }}"/>--}}
    {{--<link rel="stylesheet" href="{{ asset('assets/plugins/morrisjs/morris.min.css') }}" />--}}
    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/color_skins.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css') }}">

    {{--<link href="/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="/css/uniform.default.min.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />--}}
    <link href="/js/ng-notify/ng-notify.css" rel="stylesheet" type="text/css">
    <link id="ng_load_plugins_before" />
    {{--<link href="/css/components-md.min.css" id="style_components" rel="stylesheet" type="text/css" />--}}
    {{--<link href="/css/plugins-md.min.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="/css/todo-2.min.css" rel="stylesheet" type="text/css" />--}}

    {{--<link href="/layout/css/layout.min.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="/layout/css/default.min.css" rel="stylesheet" type="text/css" id="style_color" />--}}
    {{--<link href="/layout/css/custom.min.css" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" type="text/css" href="/js/flexslider/flexslider.css">
</head>
<body class="theme-cyan page-on-load">

<div ng-spinner-bar class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="{{ asset('assets/images/logo.svg') }}" width="48" height="48" alt="Wait..."></div>
        <p>Please wait...</p>
    </div>
</div>
<!-- END PAGE SPINNER -->
<div class="clearfix"> </div>
<!-- Page Loader -->
<div class="overlay"></div>
<!-- Top Bar -->
<nav class="navbar">
    <ul class="nav navbar-nav navbar-left">
        <li>
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="/"><img src="{{ asset('assets/images/logo-white.svg') }}" width="30" alt="Email statistics"><span class="m-l-10">Email statistics</span></a>
            </div>
        </li>
        <li>
            <a href="javascript:void(0);" class="menu-sm" data-close="true">
                <i class="zmdi zmdi-arrow-left"></i>
                <i class="zmdi zmdi-arrow-right"></i>
            </a>
        </li>
        {{--<li class="search_bar hidden-sm-down">--}}
            {{--<div class="input-group">--}}
                {{--<input type="text" class="form-control" placeholder="Search...">--}}
                {{--<span class="input-group-addon">--}}
                    {{--<i class="zmdi zmdi-search"></i>--}}
                {{--</span>--}}
            {{--</div>--}}
        {{--</li>--}}
        <li class="float-right">
            {{--<a href="javascript:void(0);" class="fullscreen hidden-md-down hidden-sm-down" data-provide="fullscreen"><i class="zmdi zmdi-fullscreen"></i></a>--}}
            {{--<a href="javascript:void(0);" class="js-right-chat"><i class="zmdi zmdi-comments"></i></a>--}}
            {{--<a href="javascript:void(0);" class="js-right-sidebar"><i class="zmdi zmdi-settings"></i></a>--}}
            {{--<a href="javascript:void(0);" class="btn_overlay hidden-sm-down"><i class="zmdi zmdi-sort-amount-desc"></i></a>--}}
            <a href="{{ route('logout') }}" class="mega-menu"><i class="zmdi zmdi-power"></i></a>
        </li>
    </ul>
</nav>
<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <div class="menu">
        <ul class="list">
            <li>
                <div class="user-info m-b-20">
                    {{--<div class="image"><a href="profile.html"><img src="../assets/images/profile_av.jpg" alt="User"></a></div>--}}
                    <div class="image"></div>
                    <div class="detail">
                        <h4>@{{user.name}}</h4>
                    </div>
                </div>
            </li>
            <li class="header">MAIN</li>
            <li ui-sref-active="active open" class="active open"> <a ui-sref="test"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
            <li ui-sref-active="active open"> <a ui-sref="all-stats({'sender_id':null})"><i class="zmdi zmdi-home"></i><span>SendGrid Stats</span></a></li>
            <li ui-sref-active="active open"> <a ui-sref="mobi2go"><i class="zmdi zmdi-home"></i><span>Mobi2Go</span></a></li>
            <li ui-sref-active="active open"> <a ui-sref="xero"><i class="zmdi zmdi-home"></i><span>Xero</span></a></li>
        </ul>
    </div>
</aside>

<!-- Main Content -->
<!-- BEGIN ACTUAL CONTENT -->
<div ui-view class="fade-in-up"> </div>
<!-- END ACTUAL CONTENT -->


<!--[if lt IE 9]>
<script src="/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script src="/js/jquery-migrate.min.js" type="text/javascript"></script>
<script src="/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/js/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/js/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/js/jquery.cokie.min.js" type="text/javascript"></script>
<script src="/js/jquery.uniform.min.js" type="text/javascript"></script>
<script src="/js/flexslider/jquery.flexslider.js"></script>
<!-- END CORE JQUERY PLUGINS -->
<!-- BEGIN CORE ANGULARJS PLUGINS -->
<script src="/js/angularjs/angular.min.js" type="text/javascript"></script>
<script src="/js/angularjs/angular-sanitize.min.js" type="text/javascript"></script>
<script src="/js/angularjs/angular-touch.min.js" type="text/javascript"></script>
<script src="/js/angularjs/plugins/angular-ui-router.min.js" type="text/javascript"></script>
<script src="/js/angularjs/plugins/ocLazyLoad.min.js" type="text/javascript"></script>
<script src="/js/angularjs/plugins/ui-bootstrap-tpls.min.js" type="text/javascript"></script>
<script src="/js/angularjs/plugins/lodash/lodash.min.js" type="text/javascript"></script>
<script src="/js/angularjs/plugins/restangular/dist/restangular.min.js" type="text/javascript"></script>
<script src="/js/angular-flexslider/angular-flexslider.js"></script>
<script src="/js/ng-notify/ng-notify.min.js"></script>
<!-- END CORE ANGULARJS PLUGINS -->
<!-- BEGIN APP LEVEL ANGULARJS SCRIPTS -->
<script src="/app/app.js" type="text/javascript"></script>
<script src="/app/routes.app.js?v=2" type="text/javascript"></script>
<script src="/app/MainController.js" type="text/javascript"></script>
<script src="/app/directives.js" type="text/javascript"></script>
<!-- END APP LEVEL ANGULARJS SCRIPTS -->
<!-- BEGIN APP LEVEL JQUERY SCRIPTS -->
<script src="/js/app.min.js" type="text/javascript"></script>
<script src="/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="/layout/scripts/demo.min.js" type="text/javascript"></script>
<!-- END APP LEVEL JQUERY SCRIPTS -->
<!-- END JAVASCRIPTS -->


<!-- Jquery Core Js -->
{{--<script src="{{ asset('assets/bundles/libscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->--}}
<script src="{{ asset('assets/bundles/vendorscripts.bundle.js') }}"></script> <!-- slimscroll, waves Scripts Plugin Js -->

{{--<script src="{{ asset('assets/bundles/morrisscripts.bundle.js') }}"></script><!-- Morris Plugin Js -->--}}
<script src="{{ asset('assets/bundles/jvectormap.bundle.js') }} "></script> <!-- JVectorMap Plugin Js -->
<script src="{{ asset('assets/bundles/knob.bundle.js') }} "></script> <!-- Jquery Knob-->

<script src="{{ asset('assets/bundles/mainscripts.bundle.js') }}"></script>
{{--<script src="{{ asset('assets/js/pages/index.js') }} "></script>--}}




</body>
</html>