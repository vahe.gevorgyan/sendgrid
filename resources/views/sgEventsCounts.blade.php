<!doctype html>
<html lang="en">
@include('header')
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<div class="container-fluid">
    <div class="row">
        @if($errors->any())
            <h4 style="color: red">{{$errors->first()}}</h4>
        @endif
    </div>
    <div class="row">
        <div class="col-2">
            <a href="{{route('testMail')}}">Home</a>
        </div>
        <div class="col-2">
            <a href="{{route('testMail')}}">Send Test</a>
        </div>
    </div>
    <div class="row">
        <h2>{{$senderName}}</h2>
    </div>
    <div class="row">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Email</th>
                <th scope="col">Processed</th>
                <th scope="col">Delivered</th>
                <th scope="col">Opened</th>
                <th scope="col">Clicked</th>
                <th scope="col">Sendgrid Message Id</th>
                <th scope="col">Last Updated</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sgEventsCounts as $sgMessageId => $events)
                <tr>
                    <td>{{$events['email']}}</td>
                    <td>@if(isset($events['processed'])){{$events['processed']}}@else 0 @endif</td>
                    <td>@if(isset($events['delivered'])){{$events['delivered']}}@else 0 @endif</td>
                    <td>@if(isset($events['open'])){{$events['open']}}@else 0 @endif</td>
                    <td>@if(isset($events['click'])){{$events['click']}}@else 0 @endif</td>
                    <th scope="row"><a href="{{route('sgMessageEvents', ['$sgMessageId' => $sgMessageId], false)}}">{{$sgMessageId}}</a></th>
                    <td>{{date("F d, Y H:i:s", $events['timestamp'])}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@include('footer')
</body>
</html>