<!doctype html>
<html lang="en">
@include('header')
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<div class="container">
    <div class="row">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        @if($errors->any())
            <h4 style="color: red">{{$errors->first()}}</h4>
        @endif
    </div>
    <div class="row">
        <div class="col">
            <a href="{{route('allEvents')}}">View All Events</a>
        </div>
    </div>
    Send test email
    <div class="row">
        <form action="{{route('sendMail')}}" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <label for="sender_name">Mail from /Sender name/:</label>
                <input type="text" class="form-control" id="sender_name" name="sender_name" placeholder="Enter Sender name">
            </div>
            <div class="form-group">
                <label for="email">Mail to:</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
            </div>
            <div class="form-group">
                <label for="text">Mail content</label>
                <textarea class="form-control" name="text" id="text" rows="3"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

@include('footer')
</body>
</html>