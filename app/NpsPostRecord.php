<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NpsPostRecord extends Model
{
    protected $fillable = [
        'mobi_webhook_id',
        'mobi_post_record_id',
        'date_confirmed',
        'date_scheduled',
        'email',
        'status',
        'post_data'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function webhook()
    {
        return $this->hasOne('App\MobiWebhook', 'id', 'mobi_webhook_id');
    }
}
