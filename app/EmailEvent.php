<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailEvent extends Model
{
    protected $fillable = [
        'timestamp',
        'event',
        'email',
        'ip',
        'sg_message_id',
        'sender_name'
    ];
}
