<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Contracts\EmailEventInterface',
            'App\Services\EmailEventService'
        );

        $this->app->bind(
            'App\Contracts\MobiInterface',
            'App\Services\MobiService'
        );

        $this->app->bind(
            'App\Contracts\NpsPostRecordInterface',
            'App\Services\NpsPostRecordService'
        );

        $this->app->bind(
            'App\Contracts\MobiPostRecordInterface',
            'App\Services\MobiPostRecordService'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
