<?php

namespace App\Http\Controllers;


use XeroPHP\Remote\URL;
use XeroPHP\Remote\Request as XeroRequest;
use XeroPHP\Application\PublicApplication;
use XeroPHP\Models\Accounting\Contact;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class XeroController extends Controller
{
    public function index(Request $request) {

            // Start a session for the oauth session storage
            session_start();
//These are the minimum settings - for more options, refer to examples/config.php
            $config = [
                'oauth' => [
                    'callback' => 'http://54.252.164.194/xero/connect',
                    'consumer_key' => 'CFZW84AFGJSLB0ZWVUGZGM3DKJX3CJ',
                    'consumer_secret' => 'LJG6OQOLNZBLMTYPKN8W267ZKQVK1C',
                ],
                'curl' => [
                    CURLOPT_CAINFO => public_path().'/certs/ca-bundle.crt',
                ],
            ];
            $xero = new PublicApplication($config);
//if no session or if it is expired
            if (null !== $oauth_session = $this->getOAuthSession()) {
                $xero->getOAuthClient()
                    ->setToken($oauth_session['token'])
                    ->setTokenSecret($oauth_session['token_secret']);

                if (isset($_REQUEST['oauth_verifier'])) {

                    $xero->getOAuthClient()->setVerifier($_REQUEST['oauth_verifier']);

                    $url = new URL($xero, URL::OAUTH_ACCESS_TOKEN);
                    $request = new XeroRequest($xero, $url);
                    $request->send();
                    $oauth_response = $request->getResponse()->getOAuthResponse();
                    $this->setOAuthSession(
                        $oauth_response['oauth_token'],
                        $oauth_response['oauth_token_secret'],
                        $oauth_response['oauth_expires_in']
                    );
                    //drop the qs
                    $uri_parts = explode('?', $_SERVER['REQUEST_URI']);
                    //Just for demo purposes
                    header(
                        sprintf(
                            'Location: http%s://%s%s',
                            (isset($_SERVER['HTTPS']) ? 's' : ''),
                            $_SERVER['HTTP_HOST'],
                            $uri_parts[0]
                        )
                    );
                    exit;
                }
                return response()->json(['status' => 'success', 'connect' => 'success'], 200);
            } else {
                return response()->json(['status' => 'success', 'connect' => 'pending'], 200);
            }




    }

    public function connect(Request $request) {
            // Start a session for the oauth session storage
            session_start();
//These are the minimum settings - for more options, refer to examples/config.php
            $config = [
                'oauth' => [
                    'callback' => 'http://54.252.164.194/xero/connect-success',
                    'consumer_key' => 'CFZW84AFGJSLB0ZWVUGZGM3DKJX3CJ',
                    'consumer_secret' => 'LJG6OQOLNZBLMTYPKN8W267ZKQVK1C',
                ],
                'curl' => [
                    CURLOPT_CAINFO => public_path().'/certs/ca-bundle.crt',
                ],
            ];
            $xero = new PublicApplication($config);
//if no session or if it is expired
            if (null === $oauth_session = $this->getOAuthSession()) {
                $url = new URL($xero, URL::OAUTH_REQUEST_TOKEN);
                $request = new XeroRequest($xero, $url);
                //Here's where you'll see if your keys are valid.
                //You can catch a BadRequestException.
                try {
                    $request->send();
                } catch (Exception $e) {
                    print_r($e);
                    if ($request->getResponse()) {
                        print_r($request->getResponse()->getOAuthResponse());
                    }
                }
                $oauth_response = $request->getResponse()->getOAuthResponse();
                \Log::info(['oauth' => $oauth_response]);
                $this->setOAuthSession(
                    $oauth_response['oauth_token'],
                    $oauth_response['oauth_token_secret']
                );
                return response()->json(['status' => 'success', 'connect' => 'pending', 'url' => $xero->getAuthorizeURL($oauth_response['oauth_token'])], 200);
            } else {
                $xero->getOAuthClient()
                    ->setToken($oauth_session['token'])
                    ->setTokenSecret($oauth_session['token_secret']);

                if (isset($_REQUEST['oauth_verifier'])) {

                    $xero->getOAuthClient()->setVerifier($_REQUEST['oauth_verifier']);

                    $url = new URL($xero, URL::OAUTH_ACCESS_TOKEN);
                    $request = new XeroRequest($xero, $url);
                    $request->send();
                    $oauth_response = $request->getResponse()->getOAuthResponse();
                    $this->setOAuthSession(
                        $oauth_response['oauth_token'],
                        $oauth_response['oauth_token_secret'],
                        $oauth_response['oauth_expires_in']
                    );
                    //drop the qs
                    $uri_parts = explode('?', $_SERVER['REQUEST_URI']);
                    //Just for demo purposes
                    header(
                        sprintf(
                            'Location: http%s://%s%s',
                            (isset($_SERVER['HTTPS']) ? 's' : ''),
                            $_SERVER['HTTP_HOST'],
                            $uri_parts[0]
                        )
                    );
                    return redirect('/#/xero');
                }

            }



    }

    public function connectSuccess() {

// Start a session for the oauth session storage
        session_start();
//These are the minimum settings - for more options, refer to examples/config.php
        $config = [
            'oauth' => [
                'callback' => 'http://54.252.164.194/xero/connect-success',
                'consumer_key' => 'CFZW84AFGJSLB0ZWVUGZGM3DKJX3CJ',
                'consumer_secret' => 'LJG6OQOLNZBLMTYPKN8W267ZKQVK1C',
            ],
            'curl' => [
                CURLOPT_CAINFO => public_path().'/certs/ca-bundle.crt',
            ],
        ];
        $xero = new PublicApplication($config);
//if no session or if it is expired
        if (null === $oauth_session = $this->getOAuthSession()) {
            $url = new URL($xero, URL::OAUTH_REQUEST_TOKEN);
            $request = new XeroRequest($xero, $url);
            //Here's where you'll see if your keys are valid.
            //You can catch a BadRequestException.
            try {
                $request->send();
            } catch (Exception $e) {
                print_r($e);
                if ($request->getResponse()) {
                    print_r($request->getResponse()->getOAuthResponse());
                }
            }
            $oauth_response = $request->getResponse()->getOAuthResponse();
            \Log::info(['oauth' => $oauth_response]);
            $this->setOAuthSession(
                $oauth_response['oauth_token'],
                $oauth_response['oauth_token_secret']
            );
            return response()->json(['status' => 'success', 'url' => $xero->getAuthorizeURL($oauth_response['oauth_token'])], 200);
        } else {
            $xero->getOAuthClient()
                ->setToken($oauth_session['token'])
                ->setTokenSecret($oauth_session['token_secret']);

            if (isset($_REQUEST['oauth_verifier'])) {

                $xero->getOAuthClient()->setVerifier($_REQUEST['oauth_verifier']);

                $url = new URL($xero, URL::OAUTH_ACCESS_TOKEN);
                $request = new XeroRequest($xero, $url);
                $request->send();
                $oauth_response = $request->getResponse()->getOAuthResponse();
                $this->setOAuthSession(
                    $oauth_response['oauth_token'],
                    $oauth_response['oauth_token_secret'],
                    $oauth_response['oauth_expires_in']
                );
                //drop the qs
                $uri_parts = explode('?', $_SERVER['REQUEST_URI']);
                //Just for demo purposes
                header(
                    sprintf(
                        'Location: http%s://%s%s',
                        (isset($_SERVER['HTTPS']) ? 's' : ''),
                        $_SERVER['HTTP_HOST'],
                        $uri_parts[0]
                    )
                );
                exit;
            }
        }
//Otherwise, you're in.
        return redirect('/#/xero');
    }

    function getOAuthSession()
    {
        //If it doesn't exist or is expired, return null
        if (! isset($_SESSION['oauth'])
            || ($_SESSION['oauth']['expires'] !== null
                && $_SESSION['oauth']['expires'] <= time())
        ) {
            return;
        }
        return $_SESSION['oauth'];
    }

    function setOAuthSession($token, $secret, $expires = null)
    {
        // expires sends back an int
        if ($expires !== null) {
            $expires = time() + (int) $expires;
        }
        $_SESSION['oauth'] = [
            'token' => $token,
            'token_secret' => $secret,
            'expires' => $expires,
        ];
    }



    public function contacts() {
        // Start a session for the oauth session storage
        session_start();
//These are the minimum settings - for more options, refer to examples/config.php
        $config = [
            'oauth' => [
                'callback' => 'http://54.252.164.194/xero/connect-success',
                'consumer_key' => 'CFZW84AFGJSLB0ZWVUGZGM3DKJX3CJ',
                'consumer_secret' => 'LJG6OQOLNZBLMTYPKN8W267ZKQVK1C',
            ],
            'curl' => [
                CURLOPT_CAINFO => public_path().'/certs/ca-bundle.crt',
            ],
        ];
        $xero = new PublicApplication($config);
//if no session or if it is expired
        if (null === $oauth_session = $this->getOAuthSession()) {
            $url = new URL($xero, URL::OAUTH_REQUEST_TOKEN);
            $request = new XeroRequest($xero, $url);
            //Here's where you'll see if your keys are valid.
            //You can catch a BadRequestException.
            try {
                $request->send();
            } catch (Exception $e) {
                print_r($e);
                if ($request->getResponse()) {
                    print_r($request->getResponse()->getOAuthResponse());
                }
            }
            $oauth_response = $request->getResponse()->getOAuthResponse();
            \Log::info(['oauth' => $oauth_response]);
            $this->setOAuthSession(
                $oauth_response['oauth_token'],
                $oauth_response['oauth_token_secret']
            );
            return response()->json(['status' => 'success', 'connect' => 'pending', 'url' => $xero->getAuthorizeURL($oauth_response['oauth_token'])], 200);
        } else {
            $xero->getOAuthClient()
                ->setToken($oauth_session['token'])
                ->setTokenSecret($oauth_session['token_secret']);

            if (isset($_REQUEST['oauth_verifier'])) {

                $xero->getOAuthClient()->setVerifier($_REQUEST['oauth_verifier']);

                $url = new URL($xero, URL::OAUTH_ACCESS_TOKEN);
                $request = new XeroRequest($xero, $url);
                $request->send();
                $oauth_response = $request->getResponse()->getOAuthResponse();
                $this->setOAuthSession(
                    $oauth_response['oauth_token'],
                    $oauth_response['oauth_token_secret'],
                    $oauth_response['oauth_expires_in']
                );
                //drop the qs
                $uri_parts = explode('?', $_SERVER['REQUEST_URI']);
                //Just for demo purposes
                header(
                    sprintf(
                        'Location: http%s://%s%s',
                        (isset($_SERVER['HTTPS']) ? 's' : ''),
                        $_SERVER['HTTP_HOST'],
                        $uri_parts[0]
                    )
                );
                exit;
            }
            $contacts = $xero->load(Contact::class)->execute();
            return response()->json(['status' => 'success', 'contacts' => $contacts], 200);

        }



    }
}
