<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Contracts\MobiInterface;
use GuzzleHttp\Client;
use App\Contracts\NpsPostRecordInterface;
use App\Contracts\MobiPostRecordInterface;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;

class MobiController extends Controller
{

    /**
     * @param Request $request
     * @param MobiInterface $mobiRepo
     * @return \Illuminate\Http\JsonResponse
     */
    public function addMobiWebhook(Request $request, MobiInterface $mobiRepo) {
        $validator = Validator::make($request->all(), [
            'location_id' => 'required|numeric',
            'location_name' => 'required',
            'api_key' => 'required',
            'post_url' => 'required',
            'company_name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()]);
        }
        $data = [
            'location_id' => $request->get('location_id'),
            'location_name' => $request->get('location_name'),
            'api_key' => $request->get('api_key'),
            'post_url' => $request->get('post_url'),
            'company_name' => $request->get('company_name'),
        ];

        $result = $mobiRepo->createOne($data);
        if($result) {
            return response()->json(['status' => 'success', 'webhook' => $result]);
        }
        return response()->json(['status' => 'error']);
    }

    /**
     * @param Request $request
     * @param MobiInterface $mobiRepo
     * @return \Illuminate\Http\JsonResponse
     */
    public function editMobiWebhook(Request $request, MobiInterface $mobiRepo) {
        $validator = Validator::make($request->all(), [
            'location_id' => 'required|numeric',
            'location_name' => 'required',
            'api_key' => 'required',
            'post_url' => 'required',
            'company_name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()]);
        }
        $data = [
            'location_id' => $request->get('location_id'),
            'location_name' => $request->get('location_name'),
            'api_key' => $request->get('api_key'),
            'post_url' => $request->get('post_url'),
            'company_name' => $request->get('company_name'),
        ];

        $result = $mobiRepo->updateOne($request->get('id'), $data);
        if($result) {
            return response()->json(['status' => 'success']);
        }
        return response()->json(['status' => 'error']);
    }

    /**
     * @param Request $request
     * @param MobiInterface $mobiRepo
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMobiWebhook(Request $request, MobiInterface $mobiRepo) {
        $result = $mobiRepo->deleteOne($request->get('id'));
        if($result) {
            return response()->json(['status' => 'success']);
        }
        return response()->json(['status' => 'error']);
    }

    /**
     * @param Request $request
     * @param MobiInterface $mobiRepo
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWebhooks(Request $request, MobiInterface $mobiRepo) {

        $allWebhooks = $mobiRepo->getAll();
        if($allWebhooks) {
            return response()->json(['status' => 'success', 'allWebhooks' => $allWebhooks]);
        }
        return response()->json(['status' => 'error']);
    }

    /**
     * @param Request $request
     * @param MobiInterface $mobiRepo
     */
    public function webHookConverter(Request $request, MobiInterface $mobiRepo, NpsPostRecordInterface $npsPostRecordRepo, MobiPostRecordInterface $mobiPostRecordRepo) {

        if($request->has('event') && $request->get('event') == 'order.confirmed') {
            if($request->get('payload')) {
                $payload = $request->get('payload');
                if(isset($payload['customer']['email']) && isset($payload['customer']['first_name']) && isset($payload['customer']['phone_number']) && isset($payload['location']['id']))
                {
                    $emailAddress = $payload['customer']['email'];
                    $firstName = $payload['customer']['first_name'];
                    $lastName = isset($payload['customer']['last_name']) ? $payload['customer']['last_name'] : '';
                    $city = isset($payload['location']['address']['city']) ? $payload['location']['address']['city'] : '';
                    $postalCode = isset($payload['location']['address']['postcode']) ? $payload['location']['address']['postcode'] : '';
                    $mobilePhone = $payload['customer']['phone_number'];

                    $webHook = $mobiRepo->getByLocationId($payload['location']['id']);
                    if($webHook) {
                        $mobiPostRecord = $mobiPostRecordRepo->createOne(['mobi_post_data' => serialize($request->all())]);
                        $convertedData = [
                            'emailAddress' => $emailAddress,
                            'contactId' => '',
                            'firstName' => $firstName,
                            'middleName' => '',
                            'lastName' => $lastName,
                            'title' => '',
                            'displayName' => '',
                            'gender' => '',
                            'dateOfBirth' => '',
                            'languageCode' => '',
                            'address1' => '',
                            'address2' => '',
                            'address3' => '',
                            'city' => $city,
                            'state' => '',
                            'postalCode' => $postalCode,
                            'countryCode' => '',
                            'company' => $webHook->company_name,
                            'mobilePhone' => $mobilePhone,
                            'homePhone' => '',
                            'workPhone' => '',
                            'note' => '',
                            'optOut' => '',
                        ];
                        $dt = Carbon::parse($payload['confirmed_at']);
                        $datetime = "$dt->year-$dt->month-$dt->day $dt->hour:$dt->minute:$dt->second";
                        $tz = $dt->timezone->getName();
                        $date_confirmed = $this->changeTimezone($datetime, $tz, 'UTC');
                        $dt->setTimezone('AEST');
                        $dt->addDays(1);
                        $dt->hour = 6;
                        $dt->minute = 0;
                        $dt->second = 0;
                        $datetime = "$dt->year-$dt->month-$dt->day $dt->hour:$dt->minute:$dt->second";
                        $date_scheduled = $this->changeTimezone($datetime, 'AEST', 'UTC');
                        $data = [
                            'mobi_webhook_id' => $webHook->id,
                            'mobi_post_record_id' => $mobiPostRecord->id,
                            'date_confirmed' => $date_confirmed,
                            'date_scheduled' => $date_scheduled,
                            'email' => $emailAddress,
                            'status' => 'Pending',
                            'post_data' => serialize($convertedData)
                        ];
                        $npsPostRecordRepo->createOne($data);
//                        $url = $webHook->post_url;
//                        $client = new Client();
//                        $response = $client->request('POST', $url, [
//                            'headers' => [
//                                'Content-Type' => 'application/json',
//                                'APIKey'     => $webHook->api_key,
//                            ],
//                            'json' => $convertedData
//                        ]);
//                        if($response->getStatusCode() == 200) {
//                            dump($response->getBody()->getContents());
//                            return response()->json(['success' => 'success'], 200);
//                        }
                    }
                    return response()->json(['success' => 'success'], 200);
                } else {
                    return response()->json(['success' => 'success', 'message' => 'missing data'], 200);
                }
            } else {
                return response()->json(['success' => 'success', 'message' => 'missing data'], 200);
            }
        }

        return response()->json(['success' => 'success'], 200);

    }

    public function getNpsRecords(Request $request, NpsPostRecordInterface $npsPostRecordRepo) {
        if($request->get('webhookId')) {
            $webhookId = true;
            $npsRecords = $npsPostRecordRepo->getAllForAudit($request->has('webhookId'));
        } else {
            $webhookId = false;
            $npsRecords = $npsPostRecordRepo->getAllForAudit();
        }
        $data = [];
        foreach($npsRecords as $npsRecord) {
            $npsRecord->date_confirmed = $this->changeTimezone($npsRecord->date_confirmed, 'UTC', 'AEST');
            $npsRecord->date_scheduled = $this->changeTimezone($npsRecord->date_scheduled, 'UTC', 'AEST');
            $npsRecord->created_at = $this->changeTimezone($npsRecord->created_at, 'UTC', 'AEST');
            if(!array_key_exists($npsRecord->webhook->company_name, $data)) {
                $data[$npsRecord->webhook->company_name]['statuses'][$npsRecord->status] = 1;
                $data[$npsRecord->webhook->company_name]['records'][] = $npsRecord;
            } else {
                $data[$npsRecord->webhook->company_name]['records'][] = $npsRecord;
                (isset($data[$npsRecord->webhook->company_name]['statuses'][$npsRecord->status])) ? $data[$npsRecord->webhook->company_name]['statuses'][$npsRecord->status] += 1 : $data[$npsRecord->webhook->company_name]['statuses'][$npsRecord->status] = 1;
            }
        }
        return response()->json(['status' => 'success', 'npsRecords' => $data, 'webhookId' => $webhookId]);

    }

    /**
     * @param Request $request
     * @param NpsPostRecordInterface $npsPostRecordRepo
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function manualPost(Request $request, NpsPostRecordInterface $npsPostRecordRepo) {

        if($request->get('id')) {
            $npsPost = $npsPostRecordRepo->getById($request->get('id'));
            $url = $npsPost->webhook->post_url;
            $client = new Client();
            try {
                $response = $client->request('POST', $url, [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'APIKey'     => $npsPost->webhook->api_key,
                    ],
                    'json' => unserialize($npsPost->post_data)
                ]);
                if($response->getStatusCode() == 200) {
                    \Log::info($response->getBody()->getContents());
                    $npsPostRecordRepo->updateStatusByObject($npsPost, ['status' => 'Passed']);
                    return response()->json(['status' => 'success']);
                } else {
                    $npsPostRecordRepo->updateStatusByObject($npsPost, ['status' => 'Failed']);
                    return response()->json(['status' => 'success']);
                }
            } catch(\Exception $e) {
                \Log::info(['webhook_post_error' => $e->getMessage()]);
                $npsPostRecordRepo->updateStatusByObject($npsPost, ['status' => 'Failed']);
                return;
            }
        } else {
            return;
        }
    }


    /**
     * @param $datetime
     * @param $from
     * @param $to
     * @return string
     */
    private function changeTimezone($datetime, $from, $to) {
        $schedule_date = new DateTime($datetime, new DateTimeZone($from));
        $date = Carbon::createFromTimestamp($schedule_date->getTimestamp())->setTimeZone($to)->formatLocalized('%Y-%m-%d %H:%M:%S');
        return $date;
    }




}
