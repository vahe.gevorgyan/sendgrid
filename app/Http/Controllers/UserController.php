<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
        }
        return redirect('/login');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser() {
        $user = Auth::user();
        //dd($user);
        return response()->json(['status' => 'success', 'user' => $user]);
    }
}
