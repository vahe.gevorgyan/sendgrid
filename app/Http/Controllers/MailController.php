<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\EmailEventInterface;
use Illuminate\Support\Facades\Validator;

class MailController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => 'postEmailEvents']);
    }
    /**
     *
     */
    public function sendMail(Request $request) {
        $validator = Validator::make($request->all(), [
            'sender_name' => 'required|string|max:255',
            'email' => 'required|email',
            'text' => 'required|string'

        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()]);
        }

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("test@example.com", $request->sender_name);
        $email->setSubject("Subject for test email");
        $email->addTo($request->email, 'Test User');
        $email->addCustomArgs(['sender_name' => $request->sender_name]);
        $email->addContent("text/plain", $request->text);
        $sendgrid = new \SendGrid(ENV('SENDGRID_API_KEY'));
        try {
            $response = $sendgrid->send($email);
            if($response->statusCode() == 202) {
                return response()->json(['status' => 'success', 'message' => 'Email was sent successfully']);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Something went wrong']);
            }
            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
    }

    /**
     * @param Request $request
     * @param EmailEventInterface $emailEventRepo
     */
    public function postEmailEvents(Request $request, EmailEventInterface $emailEventRepo)
    {
        $response = $request->all();
        $ip = '';
        $createData = [];
        foreach ($response as $value) {
            $emalArr = explode('@', $value['email']);
            $mailHostArr = explode('.', $emalArr[1]);
            if(strlen($emalArr[0]) > 2 && strlen($mailHostArr[0]) > 3) {
                $email = substr($emalArr[0],0,2).str_repeat('*',strlen($emalArr[0])-2).'*'.str_repeat('*',strlen($mailHostArr[0])-3).substr($mailHostArr[0], -3).'.'.$mailHostArr[1];
            } else {
                $email = substr($emalArr[0],0,1).str_repeat('*',strlen($emalArr[0])-1).'*'.str_repeat('*',strlen($mailHostArr[0])-1).substr($mailHostArr[0], -1).'.'.$mailHostArr[1];
            }
            $createData['event'] = $value['event'];
            $createData['email'] = $email;
            $createData['sender_name'] = $value['sender_name'];
            $createData['timestamp'] = $value['timestamp'];
            if (isset($value['ip'])) {
                $createData['ip'] = $value['ip'];
            }
            $createData['sg_message_id'] = $value['sg_message_id'];
            $emailEventRepo->createOne($createData);
        }


    }

    /**
     * @param EmailEventInterface $emailEventRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAllStats(Request $request, EmailEventInterface $emailEventRepo)
    {
        $allStats = [];
        $senderName = false;
        if($request->get('sender_id')) {
            $senderData = $emailEventRepo->getById($request->get('sender_id'));
            if(!$senderData) {
                return response()->json(['status' => 'error', 'message' => 'No such sender in database']);
            }
            $emailEvents = $emailEventRepo->getBySenderName($senderData->sender_name);
            if(!$emailEvents) {
                return response()->json(['status' => 'error', 'message' => 'No such sender name database']);
            }
            $sgMessageIds = [];
            foreach($emailEvents as $event) {
                if(!in_array($event['sg_message_id'], $sgMessageIds)) {
                    $sgMessageIds[] = $event['sg_message_id'];
                    $allStats[$event['sg_message_id']] = [];
                    $allStats[$event['sg_message_id']][$event['event']] = 1;
                    $allStats[$event['sg_message_id']]['email'] = $event['email'];
                    $allStats[$event['sg_message_id']]['timestamp'] = $event['timestamp'];
                    if(!isset($allStats[$event['sg_message_id']]['id'])) {
                        $allStats[$event['sg_message_id']]['id'] = $event['id'];
                    }
                } else {
                    if($allStats[$event['sg_message_id']]['timestamp'] < $event['timestamp']) {
                        $allStats[$event['sg_message_id']]['timestamp'] = $event['timestamp'];
                    }
                    if(isset($allStats[$event['sg_message_id']][$event['event']])) {
                        $allStats[$event['sg_message_id']][$event['event']] += 1;
                    } else {
                        $allStats[$event['sg_message_id']][$event['event']] = 1;
                    }
                }
            }
            $senderName = $senderData->sender_name;
            //return view('sgEventsCounts', ['sgEventsCounts' => $allStats, 'senderName' => $senderData->sender_name]);
        } else {
            $emailEvents = $emailEventRepo->getAll();
            $senders = [];
            foreach($emailEvents as $event) {
                if(!in_array($event['sender_name'], $senders)) {
                    $senders[] = $event['sender_name'];
                    $allStats[$event['sender_name']] = [];
                    $allStats[$event['sender_name']][$event['event']] = 1;
                    if(!isset($allStats[$event['sender_name']]['id'])) {
                        $allStats[$event['sender_name']]['id'] = $event['id'];
                    }
                } else {
                    if(isset($allStats[$event['sender_name']][$event['event']])) {
                        $allStats[$event['sender_name']][$event['event']] += 1;
                    } else {
                        $allStats[$event['sender_name']][$event['event']] = 1;
                    }
                }
            }
        }

        return response()->json(['status' => 'success', 'allStats' => $allStats, 'senderName' => $senderName]);
       // return view('userEventsCounts', ['senderEventsCounts' => $allStats]);
    }

//    public function getSgMessageEvents(EmailEventInterface $emailEventRepo, $id)
//    {
//        $senderData = $emailEventRepo->getById($id);
//        if(!$senderData) {
//            return redirect()->back()->withErrors(['No such sender in database']);
//        }
//        $emailEvents = $emailEventRepo->getBySenderName($senderData->sender_name);
//        if(!$emailEvents) {
//            return redirect()->back()->withErrors(['No such sender name database']);
//        }
//
//        $sgMessageIds = [];
//        $sgEventsCounts = [];
//        foreach($emailEvents as $event) {
//            if(!in_array($event['sg_message_id'], $sgMessageIds)) {
//                $sgMessageIds[] = $event['sg_message_id'];
//                $sgEventsCounts[$event['sg_message_id']] = [];
//                $sgEventsCounts[$event['sg_message_id']][$event['event']] = 1;
//                $sgEventsCounts[$event['sg_message_id']]['email'] = $event['email'];
//                $sgEventsCounts[$event['sg_message_id']]['timestamp'] = $event['timestamp'];
//                if(!isset($sgEventsCounts[$event['sg_message_id']]['id'])) {
//                    $sgEventsCounts[$event['sg_message_id']]['id'] = $event['id'];
//                }
//            } else {
//                if($sgEventsCounts[$event['sg_message_id']]['timestamp'] < $event['timestamp']) {
//                    $sgEventsCounts[$event['sg_message_id']]['timestamp'] = $event['timestamp'];
//                }
//                if(isset($sgEventsCounts[$event['sg_message_id']][$event['event']])) {
//                    $sgEventsCounts[$event['sg_message_id']][$event['event']] += 1;
//                } else {
//                    $sgEventsCounts[$event['sg_message_id']][$event['event']] = 1;
//                }
//            }
//
//        }
//        return view('sgEventsCounts', ['sgEventsCounts' => $sgEventsCounts, 'senderName' => $senderData->sender_name]);
//    }

    /**
     * @param EmailEventInterface $emailEventRepo
     * @param $email
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getOneSgMessageStats(Request $request, EmailEventInterface $emailEventRepo)
    {
        if($request->get('sgMessageId')) {
            $sgMessageStats = $emailEventRepo->getBySgMessageId($request->get('sgMessageId'));
            if(!$sgMessageStats) {
                return response()->json(['status' => 'error', 'message' => 'No such email in database']);
            }
            $email = '';
            foreach($sgMessageStats as $sgMessageStat) {

                if($email == '') {
                    $email = $sgMessageStat['email'];
                }
                $sgMessageStat['date'] = date("F d, Y H:i:s", $sgMessageStat['timestamp']);
            }
            return response()->json(['status' => 'success', 'sgMessageStats' => $sgMessageStats, 'email' => $email]);
        }
        return response()->json(['status' => 'error']);
    }

//    public function getSearch(Request $request, EmailEventInterface $emailEventRepo)
//    {
//        $request->validate([
//            'search' => 'required|alpha_num|max:255',
//        ]);
//        $emailEvents = $emailEventRepo->search($request->get('search'));
//        $senders = [];
//        $senderEventsCounts = [];
//        foreach($emailEvents as $event) {
//            if(!in_array($event['sender_name'], $senders)) {
//                $senders[] = $event['sender_name'];
//                $senderEventsCounts[$event['sender_name']] = [];
//                $senderEventsCounts[$event['sender_name']][$event['event']] = 1;
//                if(!isset($senderEventsCounts[$event['sender_name']]['id'])) {
//                    $senderEventsCounts[$event['sender_name']]['id'] = $event['id'];
//                }
//            } else {
//                if(isset($senderEventsCounts[$event['sender_name']][$event['event']])) {
//                    $senderEventsCounts[$event['sender_name']][$event['event']] += 1;
//                } else {
//                    $senderEventsCounts[$event['sender_name']][$event['event']] = 1;
//                }
//            }
//        }
//        return view('userEventsCounts', ['senderEventsCounts' => $senderEventsCounts, 'search' => true]);
//    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
//    public function index() {
//        return view('index');
//    }

    public function test() {
        return view('layout');
    }
}
