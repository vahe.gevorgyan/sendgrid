<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobiPostRecord extends Model
{
    protected $fillable = [
        'mobi_post_data'
    ];

}
