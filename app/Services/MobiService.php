<?php

namespace App\Services;

use App\Contracts\MobiInterface;
use App\MobiWebhook;

class MobiService implements MobiInterface
{
    /**
     * @var EmailEvent
     */
    private $mobi;

    /**
     * EmailEventService constructor.
     * @param EmailEvent $emailEvent
     */
    public function __construct(MobiWebhook $mobiWebhook)
    {
        $this->mobi = $mobiWebhook;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createOne($data)
    {
        return $this->mobi->create($data);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateOne($id, $data)
    {
        return $this->mobi->where('id', $id)->update($data);

    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->mobi->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteOne($id)
    {
        return $this->mobi->where('id', $id)->delete();
    }

    /**
     * @param $locationId
     * @return mixed
     */
    public function getByLocationId($locationId)
    {
        return $this->mobi->where('location_id', $locationId)->first();
    }

}
