<?php

namespace App\Services;

use App\Contracts\NpsPostRecordInterface;
use App\NpsPostRecord;

class NpsPostRecordService implements NpsPostRecordInterface
{
    /**
     * @var NpsPostRecord
     */
    private $npsPostRecord;

    /**
     * NpsPostRecordService constructor.
     * @param NpsPostRecord $npsPostRecord
     */
    public function __construct(NpsPostRecord $npsPostRecord)
    {
        $this->npsPostRecord = $npsPostRecord;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createOne($data)
    {
        return $this->npsPostRecord->create($data);
    }

    /**
     * @param $date
     * @return mixed
     */
    public function getAllForPost($date)
    {
        return $this->npsPostRecord->where('date_scheduled', '<=', $date)->where('status', 'Pending')->get();
    }

    /**
     * @param $object
     * @param $status
     * @return mixed
     */
    public function updateStatusByObject($object, $status)
    {
        return $object->update($status);
    }

    public function getAllForAudit($webhookId = null)
    {
        if($webhookId) {
            return $this->npsPostRecord->whereHas('webhook', function($q) use ($webhookId) {
                $q->where("id", $webhookId);
            })->with('webhook')->get();
        } else {
            return $this->npsPostRecord->with('webhook')->get();
        }

    }

    public function getById($id)
    {
        return $this->npsPostRecord->where('id', $id)->with('webhook')->first();

    }

}
