<?php

namespace App\Services;

use App\Contracts\EmailEventInterface;
use App\EmailEvent;

class EmailEventService implements EmailEventInterface
{
    /**
     * @var EmailEvent
     */
    private $emailEvent;

    /**
     * EmailEventService constructor.
     * @param EmailEvent $emailEvent
     */
    public function __construct(EmailEvent $emailEvent)
    {
        $this->emailEvent = $emailEvent;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getOne($data) {
        return $this->emailEvent->where('email', $data['email'])
            ->where('timestamp', $data['timestamp'])
            ->where('sg_message_id', $data['sg_message_id'])->first();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createOne($data) {
        return $this->emailEvent->create($data);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll() {
        return $this->emailEvent->all();
    }

    /**
     * @param $email
     * @return mixed
     */
    public function getByEmail($email) {
        return $this->emailEvent->where('email', $email)->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id) {
        return $this->emailEvent->where('id', $id)->first();
    }

    /**
     * @param $senderName
     * @return mixed
     */
    public function getBySenderName($senderName) {
        return $this->emailEvent->where('sender_name', $senderName)->get();
    }

    /**
     * @param $sgMessageId
     * @return mixed
     */
    public function getBySgMessageId($sgMessageId) {
        return $this->emailEvent->where('sg_message_id', $sgMessageId)->get();
    }

    /**
     * @param $searchData
     * @return mixed
     */
    public function search($searchData) {
        return $this->emailEvent->where('sender_name', 'like', "%$searchData%")->get();
    }

}
