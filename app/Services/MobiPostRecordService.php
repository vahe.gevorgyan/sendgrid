<?php

namespace App\Services;

use App\Contracts\MobiPostRecordInterface;
use App\MobiPostRecord;

class MobiPostRecordService implements MobiPostRecordInterface
{
    /**
     * @var MobiPostRecord
     */
    private $mobiPostRecord;

    /**
     * NpsPostRecordService constructor.
     * @param MobiPostRecord $mobiPostRecord
     */
    public function __construct(MobiPostRecord $mobiPostRecord)
    {
        $this->mobiPostRecord = $mobiPostRecord;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createOne($data)
    {
        return $this->mobiPostRecord->create($data);
    }

}
