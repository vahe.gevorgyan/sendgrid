<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Contracts\NpsPostRecordInterface;
use GuzzleHttp\Client;
use PhpParser\Error;

class NpsRecordPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nps:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(NpsPostRecordInterface $npsPostRecordRepo)
    {
        $currentDate = Carbon::now();
        $records = $npsPostRecordRepo->getAllForPost($currentDate);
        foreach($records as $record) {
            $url = $record->webhook->post_url;
            $client = new Client();
            try {
                $response = $client->request('POST', $url, [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'APIKey'     => $record->webhook->api_key,
                    ],
                    'json' => unserialize($record->post_data)
                ]);
                if($response->getStatusCode() == 200) {
                    \Log::info($response->getBody()->getContents());
                    $npsPostRecordRepo->updateStatusByObject($record, ['status' => 'Passed']);
                } else {
                    $npsPostRecordRepo->updateStatusByObject($record, ['status' => 'Failed']);
                }
            } catch(\Exception $e) {
                \Log::info(['webhook_post_error' => $e->getMessage()]);
                $npsPostRecordRepo->updateStatusByObject($record, ['status' => 'Failed']);
            }
        }
    }
}
