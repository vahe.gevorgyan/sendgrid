<?php

namespace App\Contracts;

interface EmailEventInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function getOne($data);

    /**
     * @param $data
     * @return mixed
     */
    public function createOne($data);

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $email
     * @return mixed
     */
    public function getByEmail($email);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $senderName
     * @return mixed
     */
    public function getBySenderName($senderName);

    /**
     * @param $sgMessageId
     * @return mixed
     */
    public function getBySgMessageId($sgMessageId);

    /**
     * @param $searchData
     * @return mixed
     */
    public function search($searchData);
}
