<?php

namespace App\Contracts;

interface MobiPostRecordInterface
{
    public function createOne($data);
}
