<?php

namespace App\Contracts;

interface MobiInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function createOne($data);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateOne($id, $data);

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $id
     * @return mixed
     */
    public function deleteOne($id);

    /**
     * @param $locationId
     * @return mixed
     */
    public function getByLocationId($locationId);
}
