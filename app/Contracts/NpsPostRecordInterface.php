<?php

namespace App\Contracts;

interface NpsPostRecordInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function createOne($data);

    /**
     * @param $date
     * @return mixed
     */
    public function getAllForPost($date);

    /**
     * @param $object
     * @param $status
     * @return mixed
     */
    public function updateStatusByObject($object, $status);

    public function getAllForAudit($webhookId = null);

    public function getById($id);
}
