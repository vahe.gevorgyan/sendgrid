<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobiWebhook extends Model
{
    protected $fillable = [
        'location_id',
        'location_name',
        'api_key',
        'post_url',
        'company_name'
    ];
}
