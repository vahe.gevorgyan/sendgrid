angular.module('auditApp').controller('XeroController',
    [ 		'$scope', '$rootScope', '$state', 'Restangular', '$modal', '$window', 'successStatus',
        function($scope ,  $rootScope , $state, Restangular, $modal, $window, successStatus ){
            $scope.connectSuccess = false;
            if(successStatus.connect == 'success') {
                $scope.connectSuccess = true;
            }
            $scope.connectXero = function() {
                Restangular.one('/xero/connect').get().then(function(data){
                    console.log(data);
                    if(data.status == 'success' && typeof data.url != 'undefined') {
                        $window.location.href = data.url;
                    }

                })
            }


        }]);
