angular.module('auditApp').controller('AddWebhookController',
    [ 		'$scope', '$rootScope', '$state', 'Restangular', '$modal', '$modalInstance', 'currentWebhook',
        function($scope ,  $rootScope ,  $state, Restangular, $modal, $modalInstance, currentWebhook ){

        $scope.currentWebhook = false;
        if(currentWebhook != '') {
            $scope.currentWebhook = currentWebhook.id;
            $scope.location_id = currentWebhook.location_id;
            $scope.location_name = currentWebhook.location_name;
            $scope.api_key = currentWebhook.api_key;
            $scope.post_url = currentWebhook.post_url;
            $scope.company_name = currentWebhook.company_name;
        }
            $scope.cancel = function() {
                $modalInstance.dismiss();
            };

            $scope.save = function()
            {
                $scope.messages = [];
                $scope.webhookData = {
                    location_id: $scope.location_id,
                    location_name: $scope.location_name,
                    api_key: $scope.api_key,
                    post_url: $scope.post_url,
                    company_name: $scope.company_name,
                };
                Restangular.all('/mobi/add-mobi-webhook').post($scope.webhookData).then(function(data){
                        console.log(data);
                    if(data.status != 'success') {
                        $scope.status = data.status;
                        angular.forEach(data.message, function(value, key) {
                            angular.forEach(value, function(errorText) {
                                $scope.messages.push(errorText);
                            })
                        })
                    } else {
                        $modalInstance.close({webhook: data.webhook});
                    }
                })
            }

            $scope.edit = function()
            {
                $scope.messages = [];
                $scope.webhookData = {
                    location_id: $scope.location_id,
                    location_name: $scope.location_name,
                    api_key: $scope.api_key,
                    post_url: $scope.post_url,
                    company_name: $scope.company_name,
                    id: $scope.currentWebhook
                };
                Restangular.all('/mobi/edit-mobi-webhook').post($scope.webhookData).then(function(data){
                    console.log(data);
                    if(data.status != 'success') {
                        $scope.status = data.status;
                        angular.forEach(data.message, function(value, key) {
                            angular.forEach(value, function(errorText) {
                                $scope.messages.push(errorText);
                            })
                        })
                    } else {
                        $modalInstance.close({webhook: data.webhook});
                    }
                })
            }



        }]);
