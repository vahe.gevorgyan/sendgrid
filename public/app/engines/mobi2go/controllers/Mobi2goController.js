angular.module('auditApp').controller('Mobi2goController',
    [ 		'$scope', '$rootScope', '$state', 'Restangular', '$modal', 'webhooks',
        function($scope ,  $rootScope ,  $state, Restangular, $modal, webhooks ){

            $scope.webhooks = webhooks.allWebhooks;
            console.log($scope.webhooks);

            $scope.addWebHook = function() {
                var webHook = $modal.open({
                    templateUrl: function(){
                        return '/app/engines/mobi2go/modals/add-webhook.html';
                    },
                    controller: 'AddWebhookController',
                    // backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'auditmodal show',
                    size: '900px',
                    resolve: {
                        currentWebhook: function(){
                            return '';
                        }
                    }
                });
                webHook.result.then(function () {
                    location.reload();
                }, function () {
                });
            }

            $scope.delete = function(id) {
                Restangular.all('/mobi/delete-mobi-webhook').post({id: id}).then(function(data){
                    if(data.status != 'success') {
                        $scope.status = data.status;
                        angular.forEach(data.message, function(value, key) {
                            angular.forEach(value, function(errorText) {
                                $scope.messages.push(errorText);
                            })
                        })
                    } else {
                        location.reload();
                    }
                })
            }

            $scope.edit = function(id) {
                angular.forEach($scope.webhooks, function(value, key) {
                    if(value.id == id) {
                        $scope.currentWebhook = value;
                    }
                });
                var editWebHook = $modal.open({
                    templateUrl: function(){
                        return '/app/engines/mobi2go/modals/add-webhook.html';
                    },

                    controller: 'AddWebhookController',
                    // backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'auditmodal show',
                    size: '900px',
                    resolve: {
                        currentWebhook: function(){
                            return $scope.currentWebhook;
                        }
                    }
                });
                editWebHook.result.then(function () {
                    location.reload();
                }, function () {
                });
            }



        }]);
