angular.module('auditApp').controller('SgMessageController',
    [ 		'$scope', '$rootScope', '$state', 'Restangular', 'sgMessageStats',
        function($scope ,  $rootScope ,  $state, Restangular, sgMessageStats ){

            $scope.sgMessageStats = '';
            if(sgMessageStats.status == 'success') {
                $scope.sgMessageStats = sgMessageStats.sgMessageStats;
                $scope.email = sgMessageStats.email;
            }
        }]);
