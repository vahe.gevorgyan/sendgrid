angular.module('auditApp').controller('NpsAuditController',
    [ 		'$scope', '$rootScope', '$state', 'Restangular', 'npsRecords', '$modal',
        function($scope ,  $rootScope , $state, Restangular, npsRecords, $modal ){

            $scope.webhookId = npsRecords.webhookId;
            $scope.npsRecords = npsRecords.npsRecords;
            $scope.npsLog = [];
            $scope.locationName = '';
            $scope.companyName = '';
            $scope.apiKey = '';
            $scope.locationId = '';
            $scope.urlString = '';
            if($scope.webhookId) {
                angular.forEach($scope.npsRecords, function(value, key) {
                    $scope.npsLog.push(value);
                });
                $scope.locationName = $scope.npsLog[0].records[0].webhook.location_name;
                $scope.companyName = $scope.npsLog[0].records[0].webhook.company_name;
                $scope.apiKey = $scope.npsLog[0].records[0].webhook.api_key;
                $scope.locationId = $scope.npsLog[0].records[0].webhook.location_id;
                $scope.urlString = $scope.npsLog[0].records[0].webhook.post_url;
            }
            // $scope.locationName = npsRecords.records[0].webhook.location_name;
            // $scope.companyName = npsRecords.records[0].webhook.company_name;
            // $scope.apiKey = npsRecords.records[0].webhook.api_key;
            // $scope.locationId = npsRecords.records[0].webhook.location_id;
            // $scope.urlString = npsRecords.records[0].webhook.post_url;
            console.log($scope.npsLog);

            $scope.showLog = function(val) {
                var log = $modal.open({
                    templateUrl: function(){
                        return '/app/engines/nps-audit/modals/log.html';
                    },
                    controller: 'NpsLogController',
                    // backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'auditmodal show my-modal',
                    size: '900px',
                    resolve: {
                        npsLog: function(){
                            return val;
                        }
                    }
                });
                // webHook.result.then(function () {
                //     location.reload();
                // }, function () {
                // });
            }


            $scope.manualPost = function(id) {
                Restangular.all('/mobi/manual-post').post({id:id}).then(function(data){
                    console.log(data);
                    if(data.status == 'success') {
                        $state.reload();
                    }
                })
                // webHook.result.then(function () {
                //     location.reload();
                // }, function () {
                // });
            }
            // angular.forEach(npsRecords, function(value, key) {
            //     angular.forEach(value, function(errorText) {
            //         $scope.messages.push(errorText);
            //     })
            // })

        }]);
