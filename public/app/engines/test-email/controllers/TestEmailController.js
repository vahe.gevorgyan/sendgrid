angular.module('auditApp').controller('TestEmailController',
    [ 		'$scope', '$rootScope', '$state', 'Restangular',
        function($scope ,  $rootScope ,  $state, Restangular ){


            $scope.send = function()
            {
                $scope.messages = [];
                $scope.emailData = {
                    sender_name: $scope.sender_name,
                    email: $scope.email,
                    text: $scope.text
                };
                Restangular.all('/send').post($scope.emailData).then(function(data){
                    if(data.status == 'success') {
                        $scope.messages.push(data.message);
                        $scope.status = data.status;
                    } else {
                        $scope.status = data.status;
                        angular.forEach(data.message, function(value, key) {
                            angular.forEach(value, function(errorText) {
                                $scope.messages.push(errorText);
                            })
                        })
                    }
                })
            }

        }]);
