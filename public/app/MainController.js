auditApp.controller('MainController',
	[ 		'$scope', '$rootScope', 'Restangular', '$timeout', 'ngNotify',
	function($scope,   $rootScope ,  Restangular ,  $timeout ,  ngNotify){

        Restangular.one('/user/get-user').get().then(function(data) {
            if (data && data.status == 'success') {
                console.log(data);
                $rootScope.user = data.user;
            }
        })
        console.log($rootScope.user);

}])
