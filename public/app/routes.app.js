auditApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('test', {
            url: "/",
            templateUrl: "/app/engines/test-email/views/index.html",
            controller: "TestEmailController",
            resolve: {

                news: function(Restangular){ console.log(55)
                    // return Restangular.one('/main-products').get();
                },
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'auditApp',
                        files: [
                            '/app/engines/test-email/controllers/TestEmailController.js'
                        ]
                    });
                }]
            }
        })
        .state('all-stats', {
            url: "/all-stats/:sender_id?",
            templateUrl: "/app/engines/all-stats/views/index.html",
            controller: "AllStatsController",
            resolve: {
                allStats: function(Restangular, $stateParams){
                    return Restangular.one('/all-stats').get({'sender_id' : $stateParams.sender_id});
                },
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'auditApp',
                        files: [
                            '/app/engines/all-stats/controllers/AllStatsController.js'
                        ]
                    });
                }]
            }
        })
        .state('sg-message', {
            url: "/sg-message/:sgMessageId",
            templateUrl: "/app/engines/sg-message/views/index.html",
            controller: "SgMessageController",
            resolve: {
                sgMessageStats: function(Restangular, $stateParams){
                    return Restangular.one('/sg-message').get({'sgMessageId' : $stateParams.sgMessageId});
                },
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'auditApp',
                        files: [
                            '/app/engines/sg-message/controllers/SgMessageController.js'
                        ]
                    });
                }]
            }
        })
        .state('mobi2go', {
            url: "/mobi2go",
            templateUrl: "/app/engines/mobi2go/views/index.html",
            controller: "Mobi2goController",
            resolve: {
                webhooks: function(Restangular, $stateParams){
                    return Restangular.one('/mobi/webhooks').get();
                },
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'auditApp',
                        files: [
                            '/app/engines/mobi2go/controllers/Mobi2goController.js?v=75',
                            '/app/engines/mobi2go/controllers/AddWebhookController.js?v=75'
                        ]
                    });
                }]
            }
        })
        .state('nps-audit', {
            url: "/audit/:webhookId?",
            templateUrl: "/app/engines/nps-audit/views/index.html",
            controller: "NpsAuditController",
            resolve: {
                npsRecords: function(Restangular, $stateParams){
                    return Restangular.one('/mobi/nps-records').get({'webhookId' : $stateParams.webhookId});
                },
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'auditApp',
                        files: [
                            '/app/engines/nps-audit/controllers/NpsAuditController.js'
                        ]
                    });
                }]
            }
        })
        .state('xero', {
            url: "/xero",
            templateUrl: "/app/engines/xero/views/index.html",
            controller: "XeroController",
            resolve: {
                successStatus: function(Restangular, $stateParams){
                    return Restangular.one('/xero/index').get();
                },
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'auditApp',
                        files: [
                            '/app/engines/xero/controllers/XeroController.js'
                        ]
                    });
                }]
            }
        })
        .state('xeroContacts', {
            url: "/contacts",
            templateUrl: "/app/engines/xero-contacts/views/index.html",
            controller: "XeroContactsController",
            resolve: {
                contacts: function(Restangular, $stateParams){
                    return Restangular.one('/xero/contacts').get();
                },
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'auditApp',
                        files: [
                            '/app/engines/xero-contacts/controllers/XeroContactsController.js'
                        ]
                    });
                }]
            }
        })

    $urlRouterProvider.otherwise('/');
}]);
