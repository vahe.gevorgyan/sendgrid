var FormImageCrop = function () {

    var demo1 = function() {
        $('#demo1').Jcrop();
    }
    var demo2 = function() {
        var jcrop_api;

        $('#demo2').Jcrop({
          bgColor:     'black',
          bgOpacity:   .4,
          setSelect:   [ 880, 350, 0, 50 ],
          aspectRatio: 88/35,
          //aspectRatio: false,
          allowResize: true,
          allowSelect : false,
          onChange:   showCoords,
          onSelect:   showCoords,
          onRelease:  clearCoords
        },function(){
          jcrop_api = this;
        });

        $('#coords').on('change','input',function(e){
          var x1 = $('#x1').val(),
              x2 = $('#x2').val(),
              y1 = $('#y1').val(),
              y2 = $('#y2').val();
          // jcrop_api.setSelect([x1,y1,x2,y2]);
        });

        // Simple event handler, called from onChange and onSelect
        // event handlers, as per the Jcrop invocation above
        function showCoords(c)
        {
            $('#x1').val(c.x);
            $('#y1').val(c.y);
            $('#x2').val(c.x2);
            $('#y2').val(c.y2);
            $('#w').val(c.w);
            $('#h').val(c.h);
        };

        function clearCoords()
        {
            $('#coords input').val('');
        };
    }

    var product = function() {
        var jcrop_api;

        $('#product').Jcrop({
          bgColor:     'black',
          bgOpacity:   .4,
          setSelect:   [ 400, 350, 0, 50 ],
          aspectRatio: 8/7,
          //aspectRatio: false,
          allowResize: true,
          allowSelect : false,
          onChange:   showCoords,
          onSelect:   showCoords,
          onRelease:  clearCoords
        },function(){
          jcrop_api = this;
        });

        $('#coords').on('change','input',function(e){
          var x1 = $('#x1').val(),
              x2 = $('#x2').val(),
              y1 = $('#y1').val(),
              y2 = $('#y2').val();
          //jcrop_api.setSelect([x1,y1,x2,y2]);
        });

        // Simple event handler, called from onChange and onSelect
        // event handlers, as per the Jcrop invocation above
        function showCoords(c)
        {
            $('#x1').val(c.x);
            $('#y1').val(c.y);
            $('#x2').val(c.x2);
            $('#y2').val(c.y2);
            $('#w').val(c.w);
            $('#h').val(c.h);
        };

        function clearCoords()
        {
            $('#coords input').val('');
        };
    }

    var product1 = function() {
        var jcrop_api;

        $('#product1').Jcrop({
          bgColor:     'black',
          bgOpacity:   .4,
          setSelect:   [ 400, 350, 0, 50 ],
          aspectRatio: 8/7,
          //aspectRatio: false,
          allowResize: true,
          allowSelect : false,
          onChange:   showCoords,
          onSelect:   showCoords,
          onRelease:  clearCoords
        },function(){
          jcrop_api = this;
        });

        $('#coords').on('change','input',function(e){
          var x1 = $('#x11').val(),
              x2 = $('#x22').val(),
              y1 = $('#y11').val(),
              y2 = $('#y22').val();
         // jcrop_api.setSelect([x1,y1,x2,y2]);
        });

        // Simple event handler, called from onChange and onSelect
        // event handlers, as per the Jcrop invocation above
        function showCoords(c)
        {
            $('#x11').val(c.x);
            $('#y11').val(c.y);
            $('#x22').val(c.x2);
            $('#y22').val(c.y2);
            $('#w1').val(c.w);
            $('#h1').val(c.h);
        };

        function clearCoords()
        {
            $('#coords input').val('');
        };
    }


    var handleResponsive = function() {
      if ($(window).width() <= 1024 && $(window).width() >= 678) {
        $('.responsive-1024').each(function(){
          $(this).attr("data-class", $(this).attr("class"));
          $(this).attr("class", 'responsive-1024 col-md-12');
        }); 
      } else {
        $('.responsive-1024').each(function(){
          if ($(this).attr("data-class")) {
            $(this).attr("class", $(this).attr("data-class"));  
            $(this).removeAttr("data-class");
          }
        });
      }
    }

    return {
        //main function to initiate the module
        init: function () {
            
            if (!jQuery().Jcrop) {;
                return;
            }

            App.addResizeHandler(handleResponsive);
            handleResponsive();
             demo2();
            product();
            product1()
        }

    };

}();

jQuery(document).ready(function() {
    FormImageCrop.init();
});